#include "edm1.h"
#include "graph.h"
#include <list>
#include <fstream>


void Graph::Node::pop_neighbor() const{
    _neighbors.pop_back();
}

void write(std::vector<Graph::NodeId> walk, const char *outfile)
{
    std::ofstream output;
    output.open(outfile);
    for (auto i = 0; i < walk.size(); i++)
    {
        output << walk[i] << "\n";
    }
    output.close();
}

std::vector<Graph::NodeId> eulerWalk(Graph &g, Graph::NodeId v)
{
    Graph::NodeId current_node_id = v;
    std::vector<Graph::NodeId> initial_walk = {current_node_id};
    Graph::NodeId new_node_id;
    while (g.get_node(current_node_id).adjacent_nodes().empty() == false)
    {
        new_node_id = g.get_node(current_node_id).adjacent_nodes().back().id(); // get v in (u,v)
        g.get_node(current_node_id).pop_neighbor();                             // remove (u,v)
        current_node_id = new_node_id;                                          // set cursor to v
        initial_walk.push_back(new_node_id);                                    // w = {...,v}
    }
    std::vector<Graph::NodeId> walk_out = {initial_walk.front()};
    ;
    std::vector<Graph::NodeId> walk_next = {};
    for (auto i = 1; i < initial_walk.size(); i++)
    {
        walk_next = eulerWalk(g, initial_walk[i]);
        walk_out.insert(walk_out.end(), walk_next.begin(), walk_next.end());
    }

    return walk_out;
}

bool check(Graph g)
{
    std::vector<Graph::NodeId> nodes;
    nodes.resize(g.num_nodes());
    for (auto nodeid = 0; nodeid < g.num_nodes(); nodeid++)
    {
        for (auto j = 0; j < g.get_node(nodeid).adjacent_nodes().size(); j++)
        {
            nodes[nodeid]--;
            nodes[g.get_node(nodeid).adjacent_nodes()[j].id()]++;
        }
    }
    for (auto nodeid = 0; nodeid < g.num_nodes(); nodeid++)
    {
        if (nodes[nodeid] != 0)
        {
            return false;
        }
    }
    return true;
}

void euler(Graph &g, const char *outfile)
{
    if (check(g))
    {
        std::vector<Graph::NodeId> walk = eulerWalk(g, 0);
        write(walk, outfile);
    }
    else
    {
        std::cout << "Graph ist nicht eulersch.\n";
    }
    return;
}


/*int main(int argc, char *argv[])
{
    const char *input = (argc > 1) ? argv[1] : "input.txt";
    const char *output =(argc > 2) ? argv[2] : "output.txt";
    Graph g(input, Graph::directed);
    euler(g, output);
    return 0;
}
*/