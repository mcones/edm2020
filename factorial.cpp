// factorial.cpp (Computing n! by Addition)

#include <iostream>
#include <limits>
#include "largeint.h"


LargeInt factorial(LargeInt::inputtype n)
// computes n! warning, slow: runtime O(n^3 log n)
{
    LargeInt result(1);
    for (LargeInt::inputtype i = 2; i <= n; ++i) {
        LargeInt sum(0);
        for (LargeInt::inputtype j = 1; j <= i; ++j) {
            sum += result;
        }
        result = sum;
    }
    return result;
}


int main()
{
    LargeInt::inputtype n;
    std::cout << "This program computes n!, for a natural number n up to "
              << std::numeric_limits<LargeInt::inputtype>::max() << "\n"
              << "Enter a natural number n: ";
    std::cin >> n;
    std::cout << n << "! = " << factorial(n).decimal() << "\n";
}
