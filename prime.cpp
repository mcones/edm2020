// prime.cpp (Simple Primality Test)

#include <iostream>


bool is_prime(int n)
{
    // numbers less than 2 are not prime:
    if (n < 2) {
        return false;
    }

    // check all possible divisors up to the square root of n:
    for (int i = 2; i * i <= n; ++i) {
        if (n % i == 0) {
           return false;
        }
    }
    return true;
}


int get_input()
{
    int n;
    std::cout << "This program checks whether a given integer is prime.\n"
              << "Enter an integer: ";
    std::cin >> n;
    return n;
}


void write_output(int n, bool answer)
{
    if (answer) {
        std::cout << n << " is prime.\n";
    }
    else {
        std::cout << n << " is not prime.\n";
    }
}


int main()
{
    int n = get_input();
    write_output(n, is_prime(n));
}
