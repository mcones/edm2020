// mygraph.h (Declaration of Class Graph)
#ifndef MYGRAPH_H
#define MYGRAPH_H

#include <iostream>
#include <vector>

class Graph {
public:
  using NodeId = int;  // vertices are numbered 0,...,num_nodes()-1
  using EdgeId = int;  

  class Edge {
  public:
        Edge(Graph::EdgeId e, Graph::NodeId u, Graph::NodeId v, double w);
        double edge_weight() const;
        Graph::EdgeId id() const;
        Graph::NodeId from() const;
        Graph::NodeId to() const;
  private:
        Graph::EdgeId _id;
        Graph::NodeId _head;
        Graph::NodeId _tail;
        double _edge_weight;
    };

  class Node {
  public:
        void add_adj(Graph::EdgeId e);
        void rem_adj() const;
        std::vector<Graph::EdgeId> adjacent_nodes() const;
  private:
        mutable std::vector<Graph::EdgeId> _neighbors;        
    };

  enum DirType {directed, undirected};  // enum defines a type with possible values
  Graph(NodeId num_nodes, DirType dirtype);
  Graph(char const* filename, DirType dirtype);

  void add_nodes(NodeId num_new_nodes);
  void add_edge(NodeId tail, NodeId head, double weight = 1.0);

  NodeId num_nodes() const;
  EdgeId num_edges() const;
  const Node & get_node(NodeId) const;
  const Edge & get_edge(EdgeId) const;
  void print() const;

  const DirType dirtype;
  static const NodeId invalid_node;
  static const double infinite_weight;

private:
  std::vector<Node> _nodes;
  std::vector<Edge> _edges;
};

#endif // GRAPH_H
