// fraction.h (Class Fraction)

#include <stdexcept>

class Fraction {
public:
    using inttype = long long;

    Fraction(inttype n, inttype d): _numerator(n), _denominator(d)
    {   // constructor
        if (d < 1) error_zero();
    }

    inttype numerator() const                   // return numerator
    {
        return _numerator;
    }

    inttype denominator() const                 // return denominator
    {
        return _denominator;
    }

    void reciprocal()                           // replaces a/b by b/a
    {
        if (numerator() == 0) {
            error_zero();
        } else {
            std::swap(_numerator, _denominator);
            if (denominator() < 0) {
                _numerator   = -_numerator;
                _denominator = -_denominator;
            }
        }
    }

    bool operator<(const Fraction & x) const     // comparison
    {
        return numerator() * x.denominator() < x.numerator() * denominator();
    }

    Fraction operator+(const Fraction & x) const // addition
    {
        return Fraction(numerator() * x.denominator() + 
                        x.numerator() * denominator(),
                        denominator() * x.denominator());
    }

    // further operations may be added here

private:
    inttype _numerator;
    inttype _denominator;

    void error_zero()
    {
        throw std::runtime_error("Denominator < 1 not allowed in Fraction.");
    }
};
