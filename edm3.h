#ifndef EDM3_H
#define EDM3_H

#include "mygraph.h"

class Network{
    public:
        Network(const Graph &g, Graph::NodeId s, Graph::NodeId t);
        void add_flow(Graph::EdgeId e, double val);
        void print();
        double get_excess(Graph::NodeId v);
        double get_residual(Graph::EdgeId e);

    private:
        Graph g;
        int s;
        int t;
        std::vector<double> f;
        std::vector<double> ex;
};

#endif // EDM3.H
