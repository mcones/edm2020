// collatz.cpp (Collatz Sequence)

#include <iostream>

using myint = long long;

myint get_input()
{
    myint n;
    std::cout << "This program computes the Collatz sequence for an "
              << "integer.\n" << "Enter an integer: ";
    std::cin >> n;
    return n;
}


int main()
{
    myint n = get_input();
    while (n > 1) {
        std::cout << n << "\n";
        if (n % 2 == 0) {
            n = n / 2;
        }
        else {
            n = 3 * n + 1;
        }
    }
    std::cout << n << "\n";
}
