#include <iostream>
#include <string>
#include <variant>
#include <fstream>
#include "edm1.h"
#include "edm2.h"
#include "graph.h"
#include "primdijkstra.h"


int main(int argc, char** argv){
    if (argc < 3) {
        std::cout << "Usage: ./edm2 s epsilon [input] [output]\n" << "Input/Output are optional, default is input.txt and console, respectively./n";
        return 0;
    } 
    const char * input = (argc == 3)? "input.txt" : argv[3];
    const Graph g(input, Graph::undirected);
    Graph::NodeId start = std::stoi(argv[1]);
    Graph s = shallow_light_tree(g, start, std::stof(argv[2]));
    outType out = {false};
    if (argc > 4){
        out.fd.open(argv[4]);
        out.t = true ;
        write(s, out);
        out.fd.close();
    } else{
        write(s,out);
    }
    
}

void write(const Graph & g, outType & out){
   for (auto nodeid = 0; nodeid < g.num_nodes(); ++nodeid) {
        for (auto neighbor: g.get_node(nodeid).adjacent_nodes()) {
            if (nodeid < neighbor.id()){
                if(out.t){
                    out.fd << nodeid << " " << neighbor.id() << "\n";
                } else{
                    std::cout << nodeid << " " << neighbor.id() << "\n";
                }
                
            }
        }
   }
}

void double_edges(Graph & g){
    for (auto i = 0; i< g.num_nodes();i++){
        for (auto neighbor: g.get_node(i).adjacent_nodes()){
            if (i < neighbor.id()){
                g.add_edge(i, neighbor.id(), neighbor.edge_weight());
            }
        }
    }
}

void test_print(std::vector<double> path){
    for (auto i: path){
        std::cout << i <<" ";
    }
    std::cout << "\n";
}

Graph shallow_light_tree(const Graph & g, Graph::NodeId start, float eps){
    // Make spanning tree s0
    Graph s0 = mst(g);
    Graph s = s0;
    // Make shortest path tree p
    std::vector<double> p = shortest_paths_list(g,start);
    // get euler path from doubling edges
    double_edges(s0);
    std::vector<Graph::NodeId> eulerW = eulerWalk(s0, start);
    // iterate over euler path
    std::vector<bool> visited(g.num_nodes());
    visited[0] = true;
    Graph::NodeId last_node = 0;
    double recent_path_costs = 0.0;
    for (auto node: eulerW){
        if ((not visited[node]) and (p[last_node] + recent_path_costs > (1+eps) * p[node] )){
            visited[node] = true;
            s.add_edge(start,node,p[node]);
            recent_path_costs = 0.0;
            last_node = node;
        } else{
            for (auto neighbor: g.get_node(last_node).adjacent_nodes()){
                if(neighbor.id() == node){
                    recent_path_costs += neighbor.edge_weight();
                }
            }
            
        }
    }
    return shortest_paths_tree(s,start);
}