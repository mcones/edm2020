#include <list>
#include <fstream>
#include <algorithm>
#include "edm3.h"

// Sehr unästhetischer Code, 

bool not_zero(int i){return (i!=0);}

Network::Network(const Graph & g1, Graph::NodeId s1, Graph::NodeId t1): g(g1), s(s1), t(t1){
    std::vector<double> f1;
    f = f1 ;
    std::vector<double> ex1;
    ex = ex1;
    for (auto i=0;i<g.num_edges();i++){
        f.push_back(0);
    }
    for (auto i=0;i<g.num_nodes();i++){
        ex.push_back(0);
    }
};
void Network::print(){
    std::ofstream output;
    output.open("output.txt");
    int num_flows = std::count_if(f.begin(),f.end(), not_zero);
    output << num_flows << "\n";
    for (auto i=0;i<g.num_nodes();i++)    {
        if(f[i] > 0){
            output << i << " " << f[i] << "\n";
        }
    }
    output.close();
}
void Network::add_flow(Graph::EdgeId e, double val){
    f[e] += val;
    Graph::NodeId v,w;
    w = g.get_edge(e).to();
    v = g.get_edge(e).from();
    if(t != w){
        ex[w] += val;
    }
    if(s != v ){
        ex[v] -= val;
    }
    std::cout << "Adding flow across " << e << " : " << v << "-" << w << " with value "<< val 
    << " and excess "<< ex[v] << "-" << ex[w]<< ".\n";
}

double Network::get_residual(Graph::EdgeId e){
    return g.get_edge(e).edge_weight() - f[e];
}

double Network::get_excess(Graph::NodeId v){
    return ex[v];
}

Network push_relabel(const Graph & g){
    std::vector<std::list<Graph::NodeId>> active_nodes(g.num_nodes()*2-1);
    std::vector<std::list<Graph::EdgeId>> allowed_edges(g.num_nodes());
    std::vector<Graph::NodeId> dist(g.num_nodes());
    Graph::NodeId s = 0;
    //Graph::NodeId t = g.num_nodes();
    Network n(g,0,g.num_nodes()-1);
    // (1) Init s outflows
    for (auto e: g.get_node(s).adjacent_nodes()){
        // f(e) = u(e)
        n.add_flow(e, g.get_edge(e).edge_weight());
        // mark receiving nodes active
        active_nodes[0].push_back(g.get_edge(e).to());
    }
    // (2) Init distances
    for (auto i=1;i<g.num_nodes();i++){
        dist[i] = 0;
        allowed_edges[i].clear();
    }
    dist[0] = g.num_nodes();
    // (3) Distribute flows
    int i = 0;
    Graph::NodeId v,w;
    Graph::EdgeId e;
    while(i or active_nodes[i].size()){
        if(active_nodes[i].size() == 0){
            std::cout << "No active nodes at i=" << i << ".\n";
            i--;
            continue;
        }
        v = active_nodes[i].front();

        if(allowed_edges[v].size() == 0){
            // relabel v
            std::cout << "Relabeling " << v << " to i=" << i+1 << ".\n";
            allowed_edges.clear();
            dist[v] = g.num_nodes();
            for (auto e: g.get_node(v).adjacent_nodes()){
                w = g.get_edge(e).to();
                if (dist[w]+1 < dist[v]){
                    dist[v] = dist[w]+1;
                }
            }
            // Update allowed edges
            for (auto e: g.get_node(v).adjacent_nodes()){
                w = g.get_edge(e).to();
                std::cout << "Checking edge " << e << ":" << v << "-" << w <<" to "<< w << " with dist v-w " << dist[v] << "-" << dist[w]<< ".\n";
                if (dist[v] == dist[w]+1){
                    std::cout << "Adding allowed edge to "<< w <<".\n";
                    allowed_edges[v].push_back(e);
                }
            }
            // Move v from L_i to L_i+1
            active_nodes[i].pop_front();
            active_nodes[i+1].push_back(v);
            i++;
            //TODO: Remove no longer allowed edges (u,v)?
        } else {
            // push(e)
            e = allowed_edges[v].front();
            w = g.get_edge(e).to();
            double gamma = std::min(n.get_residual(e),n.get_excess(v));
            if (gamma == n.get_residual(e)) {
                allowed_edges[v].pop_front();
            }
            std::cout << "Old excess: v: "<< n.get_excess(v) << ", w: "<< n.get_excess(w) <<".\n";
            n.add_flow(e,gamma);
            // update active nodes
            std::cout << "Pushing "<< gamma << " from "<< v << " to "<< w <<".\n";
            std::cout << "New excess: v: "<< n.get_excess(v) << ", w: "<< n.get_excess(w) <<".\n";
            if (n.get_excess(w)>0){
                active_nodes[dist[w]].push_back(w);
            }
            if (n.get_excess(v)<=0){
                active_nodes[i].pop_front();
            }
        }
    }
    return n;
}

int main(int argc, char** argv){
    const char * input = (argc < 2)? "input.txt" : argv[1];
    //const char * _output = "output.txt";
    const Graph g(input, Graph::directed);
    g.print();
    Network n = push_relabel(g);
    n.print();
    return 0;
    
}