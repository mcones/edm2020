// harmonic.cpp (Harmonic Numbers)

#include <iostream>
#include <stdexcept>
#include "fraction.h"

int main()
{
try {
    std::cout << "This program computes H(n)=1/1+1/2+1/3+...+1/n.\n"
              << "Enter an integer n: ";
    Fraction::inttype n;
    std::cin >> n;
    Fraction sum(0,1);
    for (Fraction::inttype i = 1; i <= n; ++i) {
        sum = sum + Fraction(1, i);
    }
    std::cout << "H(" << n << ") = "
              << sum.numerator() << "/" << sum.denominator() << " = "
              << static_cast<double>(sum.numerator()) / sum.denominator() << "\n";
}
catch(std::runtime_error e) {
    std::cout << "RUNTIME ERROR: " << e.what() << "\n";
    return 1;
}
}
