#ifndef EDM1_H
#define EDM1_H

#include "graph.h"

void euler(Graph &g, const char * outfile);
std::vector<Graph::NodeId> eulerWalk(Graph &g, Graph::NodeId v);
void write(std::vector<Graph::NodeId>walk, const char* outfile);

#endif // EDM1.H
