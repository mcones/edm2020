#ifndef EDM2_H
#define EDM2_H

#include "graph.h"

struct outType{
    bool t; 
    std::ofstream fd;
};

void write(const Graph & g, outType & out);
void double_edges(Graph & g);
Graph shallow_light_tree(const Graph & g, Graph::NodeId start, float eps);

#endif // EDM2.H
