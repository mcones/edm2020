// testgraph.cpp (Read Digraph from File and Print)

#include "graph.h"

int main(int argc, char* argv[])
{
    if (argc > 1) {
        Graph g(argv[1], Graph::directed);
        g.print();
    }
}
