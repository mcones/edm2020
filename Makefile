CC=clang++
NAME=edm3
OBJECTS=edm3.o mygraph.o
HEADERS=edm3.h mygraph.h
CFLAGS=-g -Wall 
LDFLAGS=-g -Wall

%.o: %.cpp $(HEADERS)
	$(CC) -c -o $@ $< $(CFLAGS)
        
$(NAME):$(OBJECTS) $(HEADERS)
	$(CC) -o $@ $(OBJECTS) $(LDFLAGS)
        
run:
	./$(NAME)
        
clean:
	rm $(OBJECTS) $(NAME)
        
debug:
	gdb $(NAME)
