// square.cpp (Compute the Square of an Integer)

#include <iostream>


int main()
{
    std::cout << "Enter an integer: ";
    int x;
    std::cin >> x;
    int result = x * x;
    std::cout << "The square of " << x << " is " << result << ".\n";
}
