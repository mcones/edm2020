// euclid.cpp (Euclidean Algorithm)

#include <iostream>

using myint = long long;

myint gcd(myint a, myint b)         // compute greatest common divisor
{                                   // using Euclidean algorithm
    if (b == 0) {
        return a;
    }
    else {
        return gcd(b, a % b);
    }
}


int main()
{
    myint a, b;
    std::cout << "This program computes the greatest common divisor.\n"
              << "Enter two natural numbers, separated by blank: ";
    std::cin >> a >> b;
    std::cout << "gcd(" << a << "," << b << ") = " << gcd(a,b) << "\n";
}
