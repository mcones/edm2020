// heapsort.cpp (Example for Heapsort)

#include <iostream>
#include <cstdlib>
#include "heap.h"

int main()
{
    int n;
    std::cout << "How many numbers do you want to sort? ";
    std::cin >> n;

    Heap<int> heap;
    int new_number;

    std::cout << "I will sort the following " << n << " numbers:\n";
    for (int i = 0; i < n; ++i) {
        new_number = rand() % 900 + 100;
        std::cout << new_number << " ";
        heap.insert(new_number);
    }

    std::cout << "\n" << "Sorted:\n";
    while (not(heap.is_empty())) {
        std::cout << heap.extract_min() << " ";
    }
    std::cout << "\n";
}
