// baseconv.cpp (Integer Base Converter)

#include <iostream>
#include <string>
#include <limits>

const std::string hexdigits = "0123456789ABCDEF";

std::string b_ary_representation(int base, int number)
// returns the representation of "number" with base "base", assuming 2<=base<=16.
{
    if (number > 0) {
        return b_ary_representation(base, number / base) + hexdigits[number % base];
    }
    else {
        return "";
    }
}


bool get_input(int & base, int & number)           // call by reference
{
    std::cout << "This program computes the representation of a natural number"
              << " with respect to a given base.\n"
              << "Enter a base among 2,...," << hexdigits.size() << " : ";
    std::cin >> base;
    std::cout << "Enter a natural number among 1,...,"
              << std::numeric_limits<int>::max() << " : ";
    std::cin >> number;
    return (base > 1) and (base <= hexdigits.size()) and (number > 0);
}


int main()
{
    int b, n;
    if (get_input(b, n)) {
        std::cout << "The " << b << "-ary representation of " << n
                  << " is " << b_ary_representation(b, n) << ".\n";
    }
    else std::cout << "Sorry, wrong input.\n";
}
