#ifndef PRIMDIJKSTRA_H
#define PRIMDIJKSTRA_H

#include "graph.h"
#include "heap.h"

struct HeapItem;

bool operator<(const HeapItem & a, const HeapItem & b);

class NodeHeap : public Heap<HeapItem> {
public:
    bool is_member(Graph::NodeId nodeid);
    double get_key(Graph::NodeId nodeid);
    Graph::NodeId extract_min();
    void insert(Graph::NodeId nodeid, double key);
    void decrease_key(Graph::NodeId nodeid, double new_key);
    void remove(Graph::NodeId nodeid);

private:
    void ensure_is_valid_nodeid(Graph::NodeId nodeid) const;
    void swap(HeapItem & a, HeapItem & b);
    static const int not_in_heap;
    std::vector<int> _heap_node;
};

struct PrevData;

Graph mst(const Graph & g);
Graph shortest_paths_tree(const Graph & g, Graph::NodeId start_nodeid);
std::vector<double> shortest_paths_list(const Graph & g, Graph::NodeId start_nodeid);

#endif // PRIMDIJKSTRA_H